package com.digitalml.rest.resources.codegentest.application;

import com.digitalml.rest.resources.codegentest.resource.TyreResource;
import com.digitalml.rest.resources.codegentest.resource.ResourceLoggingFilter;					
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import com.codahale.metrics.health.HealthCheck;
import io.dropwizard.Configuration;

import java.util.EnumSet;
import javax.servlet.DispatcherType;

import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.util.ContextInitializer;
import ch.qos.logback.core.joran.spi.JoranException;


public class TyreApplication extends Application<TyreConfiguration> {

	public static void main(final String[] args) throws Exception {
		new TyreApplication().run(args);
	}
	
	@Override
	public void initialize(final Bootstrap<TyreConfiguration> bootstrap) {
		// TODO: application initialization
	}

	@Override
	public void run(final TyreConfiguration configuration, final Environment environment) {

		//separate logging setup to enable use of external logback xml
		LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
		context.reset();
		ContextInitializer initializer = new ContextInitializer(context);
		try {
			initializer.autoConfig();
		} catch (JoranException e) {
		}

		final TyreResource resource = new TyreResource();
		final TyreHealthCheck healthCheck = new TyreHealthCheck();
		
		environment.servlets().addFilter("ResourceLoggingFilter", new ResourceLoggingFilter()).addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");
		
		environment.healthChecks().register("template", healthCheck);
		environment.jersey().register(resource);

	}
}

final class TyreConfiguration extends Configuration {
}

final class TyreHealthCheck extends HealthCheck {

	public TyreHealthCheck() {
	}

	@Override
	protected Result check() throws Exception {
		return Result.healthy();
	}
}