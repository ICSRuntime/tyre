package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.*;

/*
JSON Representation for Response:
{
  "required": [
    "tyre"
  ],
  "type": "object",
  "properties": {
    "tyre": {
      "type": "string"
    }
  }
}
*/

public class Response {

	@Size(max=1)
	@NotNull
	private String tyre;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    tyre = org.apache.commons.lang3.StringUtils.EMPTY;
	}
	public String getTyre() {
		return tyre;
	}
	
	public void setTyre(String tyre) {
		this.tyre = tyre;
	}
}