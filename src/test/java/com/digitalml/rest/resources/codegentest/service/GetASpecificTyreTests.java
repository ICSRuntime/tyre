package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Tyre.TyreServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TyreService.GetASpecificTyreInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TyreService.GetASpecificTyreReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetASpecificTyreTests {

	@Test
	public void testOperationGetASpecificTyreBasicMapping()  {
		TyreServiceDefaultImpl serviceDefaultImpl = new TyreServiceDefaultImpl();
		GetASpecificTyreInputParametersDTO inputs = new GetASpecificTyreInputParametersDTO();
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		GetASpecificTyreReturnDTO returnValue = serviceDefaultImpl.getaspecifictyre(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}